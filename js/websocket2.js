var output = document.getElementById("output");
var form = document.getElementById("form");
var username;
var useremail;
var allUsersforNotifications;
var newUserforNotifications;
var recidCountUser;
var recidCountStrategy;
var recidCountNotification;
var Strategies;
var Scripts;
var newStrategies;
var countStrategy = 0;
var newStrategiesArray = [];
var ID;
var NameStrategy;
var Indicator;
var Signals;
var FillBars;
var password;
var buyArrowSignall = "0";
var sellArrowSignall = "0";
var PlotSignalSizeBuy;
var PlotSignalSizeSell;
var EditNameStrategy = [];
var ArrayStrategies;
var ObjectEditStrategy;
var RealTimeEditedStrategy;
var StrategyName;
var BuyScript;
var SellScript;
var BarBuyColor;
var BarSellColor;
var CheckConditions;
var FillBarsEdit;
var PlotIndicator;
var PlotSignal;
var PlotArrowTypeBuy;
var PlotArrowTypeSell;
var PlotSignalColorBuy;
var PlotSignalColorSell;
var Parameters;
var EditedStrategyID;
var EditedStrategyID2;
var EditedStrategyName;
var EditedBuyScript;
var EditedSellScript;
var EditedBarBuyColor;
var EditedBarSellColor;
var EditedCheckConditions;
var EditedFillBarsEdit;
var EditedPlotIndicator;
var EditedPlotSignal;
var EditedPlotArrowTypeBuy;
var EditedPlotArrowTypeSell;
var EditedPlotSignalColorBuy;
var EditedPlotSignalColorSell;
var EditedPlotSignalSizeBuyEdit;
var EditedPlotSignalSizeSellEdit;
var editCounter = 0;
var LinesId = [];
var EnabledLines = [];
var LineName = [];
var LineType = [];
var LineSize = [];
var ColorLines = [];
var showOutputScript;
var notificationName;
var Expiration;
var bytes = [];
var maxCheckboxUserValue;
var validNewPassword = false;
var validRepeatNewPassword = false;
var patternPassword = /(?=^.{4,}$)/i;
var userStrategies;
var userScripts;
var userStrategiesForExpiration = [];
var ExpirationUserStrategies = [];
var ArrayUserStrategies = [];
var UserNameForEditStrategy = [];
var lockedStrategyPassword = "";
var EnabledStrategy;
var createdStrategy;
var EditedUserStrategyId;
var EditedUserName;
var flag = 0;
var flag2;
var websocket;
function init() {
    $("#createScript .modal-footer button:first-child").on("click", function() {
        if ($("#ScriptName").val() !== "") {
            if ($("#createScript textarea").val() !== "") {
                var ValidScript = {
                    Script: $("#createScript textarea").val(),
                    Operation: "ValidScriptRequest"
                };
                websocket.send(JSON.stringify(ValidScript));
                flag2 = 0
            } else alert("Script field cannot be empty")
        } else alert("Name field cannot be empty")
    });
    $("#editScriptModal .modal-footer button:first-child").on("click", function() {
        if ($("#EditedScriptName").val() !== "") {
            if ($("#editScriptModal textarea").val() !== "") {
                var ValidScript = {
                    Script: $("#editScriptModal textarea").val(),
                    Operation: "ValidScriptRequest"
                };
                websocket.send(JSON.stringify(ValidScript));
                flag2 = 1
            } else alert("Script field cannot be empty")
        } else alert("Name field cannot be empty")
    });
    $('#textBuyScript').bind('input propertychange', function() {
        flag = 1
    });
    $('#textSellScript').bind('input propertychange', function() {
        flag = 1
    });
    $("#lockedPassword").on("click", function() {
        var id = $("tr.w2ui-selected").attr("recid");
        lockedStrategyPassword = $("input#lockPsw").val();
        var lockStrategy = {
            Id: id,
            Password: $("input#lockPsw").val(),
            Operation: "GetLockedStrategyRequest"
        };
        console.log(lockStrategy);
        websocket.send(JSON.stringify(lockStrategy))
    });
    $("#newPsw").blur(function() {
        if ($("#newPsw").val() === "") $("#messageNewPassword").text("The field repeat password should not be empty");
        else if (!$("#newPsw").val().match(patternPassword)) $("#messageNewPassword").text("The password must least four characters");
        else {
            $("#messageNewPassword").text("");
            validNewPassword = true
        }
    });
    $("#repeatPsw").blur(function() {
        if ($("#repeatPsw").val() === '') $("#messageRepeatNewPassword").text("The field repeat password should not be empty");
        else if ($("#repeatPsw").val() !== $("#newPsw").val()) $("#messageRepeatNewPassword").text("Passwords do not match");
        else {
            $("#messageRepeatNewPassword").text("");
            validRepeatNewPassword = true
        }
    });

    function toUTF8Array(str) {
        var utf8 = [];
        for (var i = 0; i < str.length; i++) {
            var charcode = str.charCodeAt(i);
            if (charcode < 0x80) utf8.push(charcode);
            else if (charcode < 0x800) {
                utf8.push(0xc0 | (charcode >> 6), 0x80 | (charcode & 0x3f))
            } else if (charcode < 0xd800 || charcode >= 0xe000) {
                utf8.push(0xe0 | (charcode >> 12), 0x80 | ((charcode >> 6) & 0x3f), 0x80 | (charcode & 0x3f))
            } else {
                i++;
                charcode = 0x10000 + (((charcode & 0x3ff) << 10) | (str.charCodeAt(i) & 0x3ff));
                utf8.push(0xf0 | (charcode >> 18), 0x80 | ((charcode >> 12) & 0x3f), 0x80 | ((charcode >> 6) & 0x3f), 0x80 | (charcode & 0x3f))
            }
        }
        return utf8
    }
    document.querySelector("input[type='file']").addEventListener('change', function() {
        var reader = new FileReader();
        reader.onload = function() {
            var arrayBuffer = this.result,
                array = new Uint8Array(arrayBuffer),
                binaryString = String.fromCharCode.apply(null, array);
            console.log(toUTF8Array(binaryString));
            var ImportStrategy = {
                Operation: "ImportStrategyRequest",
                Strategy: toUTF8Array(binaryString)
            };
            websocket.send(JSON.stringify(ImportStrategy));
            w2ui["strategyGrid"].clear()
        };
        reader.readAsArrayBuffer(this.files[0])
    }, false);
    $("#export").click(function() {
        var Export = {
            Operation: "ExportStrategyRequest"
        };
        websocket.send(JSON.stringify(Export))
    });
    $(".buySellSignals .sellSignall .dropdown ul.dropdown-menu li").on("click", function() {
        buyArrowSignall = $(this).attr("value");
        $("#dropdownMenu1 span").replaceWith($(this).html())
    });
    $(".buySellSignals .buySignall .dropdown ul.dropdown-menu li").on("click", function() {
        sellArrowSignall = $(this).attr("value");
        $("#dropdownMenu2 span").replaceWith($(this).html())
    });
    $("select[name='SignalSizeBuy']").on('change', function() {
        PlotSignalSizeBuy = $("select[name='SignalSizeBuy'] option:selected").text()
    });
    $("select[name='SignalSizeSell']").on('change', function() {
        PlotSignalSizeSell = $("select[name='SignalSizeSell'] option:selected").text()
    });
    testWebSocket()
}

function fromArgb(num) {
    var hexString = ((num) >>> 0).toString(16).slice(-8);
    return hexString.substr(2, hexString.length)
}

function toArgb(hex) {
    var bbggrr = "ff" + hex.substr(0, 2) + hex.substr(2, 2) + hex.substr(4, 2);
    var intValue = (parseInt(bbggrr, 16)) << 32;
    return intValue
}

function componentToHex(color) {
    var hex = color.toString(16);
    return hex.length == 1 ? "0" + hex : hex
}

function rgbToHex(r, g, b) {
    return componentToHex(r) + componentToHex(g) + componentToHex(b)
}

function testWebSocket() {
    var username = document.getElementById("loginUsername").value;
    var password = document.getElementById("loginPassword").value;
    var newUrl = wsUri + "/?username=" + username + "&apikey=" + password;
    websocket = new WebSocket(newUrl);
    websocket.onopen = function(evt) {
        onOpen(evt)
    };
    websocket.onclose = function(evt) {
        onClose(evt)
    };
    websocket.onmessage = function(evt) {
        onMessage(evt)
    };
    websocket.onerror = function(evt) {
        onError(evt)
    }
}

function onOpen(evt) {
    console.log("CONNECTED");
    loginRequest();
    getAllUsersRequest();
    getAllNotificationsRequest();
    getAllStrategiesRequest();
    getAllScriptRequest()
}

function onClose(evt) {
    console.log("DISCONNECTED")
}

function onMessage(evt) {
    var text = "";
    var msg = JSON.parse(evt.data);
    console.log(msg);
    if (msg === "Bad Credentials") {
        alert("Inncorect Login or Password");
        window.location.reload()
    }
    if (msg === "Cannot create user" + "\n" + "A duplicate value cannot be inserted into a unique index. [ Table name = User,Constraint name = PK__User__000000000000001C ]") alert("User already exist!");
    if (msg[0] === "E" && msg[4] === "r") {
        alert(msg)
    };
    switch (msg.Operation) {
        case "GetAllUsersResponse":
            allUsersforNotifications = msg.Users;
            for (var i = 0; i < msg.Users.length; i++) {
                var UserName = msg.Users[i].UserName;
                var Email = msg.Users[i].Email;
                allUsers.push(msg.Users[i]);
                UserNameForEditStrategy[i] = msg.Users[i].UserName;
                userStrategies = msg.Users[i].UserStrategies;
                userScripts = msg.Users[i].UserScripts;
                if (userStrategies.length !== 0) {
                    for (var j = 0; j < userStrategies.length; j++) {
                        ExpirationUserStrategies[j] = userStrategies[j].ExpirationDate;
                        ArrayUserStrategies = userStrategies[j].Strategy
                    }
                }
                w2ui['usersGrid'].add({
                    recid: i,
                    colUsername: UserName,
                    colEmail: Email
                });
                $("#checkAll").append("<p class='usersCheckbox' id='" + UserName + "'><input type='checkbox' value='" + UserName + "'>" + UserName + "</p>")
            }
            maxCheckboxUserValue = Math.max(i);
            break;
        case "GetAllStrategiesResponse":
            Strategies = msg.Strategy;
            for (var i = 0; i < msg.Strategy.length; i++) {
                ID = msg.Strategy[i].ID;
                EnabledStrategy = msg.Strategy[i].IsEnabled;
                showOutputScript = msg.Strategy[i].Parameters;
                var Name = msg.Strategy[i].Name;
                var PlotIndicator = msg.Strategy[i].PlotParameters.PlotIndicator;
                var PlotSignal = msg.Strategy[i].PlotParameters.PlotSignal;
                var PlotFillBars = msg.Strategy[i].PlotParameters.FillBars;
                var CheckConditions = msg.Strategy[i].PlotParameters.CheckConditions;
                allStrategies.push(msg.Strategy[i]);
                w2ui['strategyGrid'].add({
                    recid: ID,
                    enabled: EnabledStrategy,
                    name: Name,
                    indicator: PlotIndicator,
                    signals: PlotSignal,
                    fillbars: PlotFillBars
                });
                $("#strategyTable").append("<tr id='" + ID + "'><td style='display: none;'>" + ID + "</td><td class='tdEnabled' id=" + ID + "><input type='checkbox' id='" + ID + "'></td><td class='tdStrategyName'>" + Name + "</td><td class='tdExpirationDate'><input type='text' id = 'date" + i + "' class='datepicker2'</td></tr>")
            }
            break;
        case "GetAllNotificationsResponse":
            for (var i = 0; i < msg.Notifications.length; i++) {
                var Name = msg.Notifications[i].Name;
                var Expiration = msg.Notifications[i].ExpirationDate;
                allNotifications.push(msg.Notifications);
                w2ui['notificationGrid'].add({
                    recid: i,
                    name: Name,
                    expiration: Expiration
                })
            }
            break;
        case "GetAllScriptsResponse":
            Scripts = msg.Script;
            console.log(Scripts);
            for (var i = 0; i < msg.Script.length; i++) {
                var ID = msg.Script[i].ID;
                var Name = msg.Script[i].Name;
                var IsFree = msg.Script[i].IsFree;
                w2ui['ScriptGrid'].add({
                    recid: ID,
                    name: Name,
                    isfree: IsFree
                });
                $("#scriptTable").append("<tr id='" + ID + "'><td style='display: none;'>" + ID + "</td><td class='tdScriptEnabled' id=" + ID + "><input type='checkbox' id='" + ID + "'></td><td class='tdScriptName'>" + Name + "</td></tr>")
            }
            break;
        case "CreateUserResponse":
            var newUser = msg.User;
            newUserforNotifications = newUser;
            username = newUser.UserName;
            useremail = newUser.Email;
            recidCountUser = w2ui['usersGrid'].total;
            w2ui['usersGrid'].add({
                recid: recidCountUser,
                colUsername: username,
                colEmail: useremail
            });
            $("#checkAll").append("<p class='usersCheckbox' id='" + username + "'><input type='checkbox' value='" + username + "'>" + username + "</p>");
            $("#addUser").modal("hide");
            break;
        case "EditUserStrategyResponse":
            var editedUserStrategy = msg.Strategy;
            console.log(editedUserStrategy);
            break;
        case "ChangePasswordResponse":
            var changedPassword = msg.User;
            console.log(changedPassword);
            break;
        case "RemoveUserResponse":
            var removeUser = msg.User;
            username = removeUser.UserName;
            $("p#" + username + "").remove();
            var deleteUserId = $($("div[title='" + username + "'").parent()).parent().attr("recid");
            w2ui['usersGrid'].remove(deleteUserId);
            console.log(removeUser);
            break;
        case "CreateStrategyResponse":
            newStrategies = msg.Strategy;
            ID = newStrategies.ID;
            console.log(ID);
            NameStrategy = newStrategies.Name;
            EnabledStrategy = newStrategies.IsEnabled;
            FillBars = newStrategies.PlotParameters.FillBars;
            Signals = newStrategies.PlotParameters.PlotSignal;
            Indicator = newStrategies.PlotParameters.PlotIndicator;
            newStrategiesArray = newStrategiesArray.concat(newStrategies);
            recidCountStrategy = w2ui['strategyGrid'].total;
            if (newStrategies !== undefined) Strategies = Strategies.concat(newStrategies);
            w2ui['strategyGrid'].add({
                recid: ID,
                enabled: EnabledStrategy,
                name: NameStrategy,
                indicator: Indicator,
                signals: Signals,
                fillbars: FillBars
            });
            $("#strategyTable").append("<tr id='" + ID + "'><td style='display: none;'>" + ID + "</td><td class='tdEnabled'><input type='checkbox' id='" + ID + "'></td><td class='tdStrategyName'>" + NameStrategy + "</td><td class='tdExpirationDate'><input type='text' id = 'date" + recidCountStrategy + "' class='datepicker2'</td></tr>");
            forDatePicker();
            countStrategy++;
            $("#grid_strategyGrid_records input[type='checkbox']").attr("disabled", true);
            break;
        case "DeleteStrategyResponse":
            var deleteStrategyId;
            var deleteStrategy = msg.Strategy;
            deleteStrategyId = deleteStrategy.ID;
            console.log(deleteStrategyId);
            w2ui['strategyGrid'].remove(deleteStrategyId);
            console.log($("#strategyTable tr#" + deleteStrategyId));
            $("#strategyTable tr#" + deleteStrategyId).remove();
            for (var i = 0; i < Strategies.length; i++) {
                if (deleteStrategyId === Strategies[i].ID) Strategies.splice(i, 1)
            }
            $("#grid_strategyGrid_records input[type='checkbox']").attr("disabled", true);
            console.log(deleteStrategy);
            break;
        case "EditStrategyResponse":
            var editedStrategy = msg.Strategy;
            RealTimeEditedStrategy = editedStrategy;
            EditedStrategyID = editedStrategy.ID;
            EditedStrategyName = editedStrategy.Name;
            EnabledStrategy = editedStrategy.IsEnabled;
            EditedBuyScript = editedStrategy.BuyScript;
            EditedSellScript = editedStrategy.SellScript;
            EditedBarBuyColor = editedStrategy.PlotParameters.BarBuyColor;
            EditedBarSellColor = editedStrategy.PlotParameters.BarSellColor;
            EditedCheckConditions = editedStrategy.PlotParameters.CheckConditions;
            EditedFillBarsEdit = editedStrategy.PlotParameters.FillBars;
            EditedPlotIndicator = editedStrategy.PlotParameters.PlotIndicator;
            EditedPlotSignal = editedStrategy.PlotParameters.PlotSignal;
            EditedPlotArrowTypeBuy = editedStrategy.PlotParameters.PlotArrowTypeBuy;
            EditedPlotArrowTypeSell = editedStrategy.PlotParameters.PlotArrowTypeSell;
            EditedPlotSignalColorBuy = editedStrategy.PlotParameters.PlotSignalColorBuy;
            EditedPlotSignalColorSell = editedStrategy.PlotParameters.PlotSignalColorSell;
            EditedPlotSignalSizeBuyEdit = editedStrategy.PlotParameters.PlotSignalSizeBuy;
            EditedPlotSignalSizeSellEdit = editedStrategy.PlotParameters.PlotSignalSizeSell;
            var id = $('tr.w2ui-selected').attr('recid');
            $("input#checkConditions").attr("checked", EditedCheckConditions);
            $("input#fillBars").attr("checked", EditedFillBarsEdit);
            $("input#PlotIndicatorID").attr("checked", EditedPlotIndicator);
            $("input#PlotSignalID").attr("checked", EditedPlotSignal);
            ArrayStrategies = Strategies;
            if (newStrategies !== undefined) {
                newStrategiesArray = Strategies.concat(newStrategies)
            }
            for (var i = 0; i < ArrayStrategies.length; i++) {
                EditedStrategyID2 = ArrayStrategies[i].ID;
                if (EditedStrategyID !== undefined) {
                    if (EditedStrategyID == EditedStrategyID2) ArrayStrategies[i] = RealTimeEditedStrategy
                }
            }
            $("#strategyTable tr[id='" + EditedStrategyID + "'] td.tdStrategyName").html(EditedStrategyName);
            w2ui['strategyGrid'].set(EditedStrategyID, {
                recid: EditedStrategyID,
                enabled: EnabledStrategy,
                name: EditedStrategyName,
                indicator: EditedPlotIndicator,
                signals: EditedPlotSignal,
                fillbars: EditedFillBarsEdit
            });
            console.log(editedStrategy);
            $("#grid_strategyGrid_records input[type='checkbox']").attr("disabled", true);
            break;
        case "RemoveNotificationResponse":
            var removeNotification = msg.Notification;
            var removeNotificationName;
            removeNotificationName = removeNotification.Name;
            var deleteNotificationId = $($("div[title='" + removeNotificationName + "'").parent()).parent().attr("recid");
            w2ui['notificationGrid'].remove(deleteNotificationId);
            console.log(removeNotification);
            break;
        case "CreateNotificationResponse":
            var newNotification = msg.Notification;
            notificationName = newNotification.Name;
            Expiration = newNotification.ExpirationDate;
            recidCountNotification = w2ui['notificationGrid'].total;
            w2ui['notificationGrid'].add({
                recid: recidCountNotification,
                name: notificationName,
                expiration: Expiration
            });
            break;
        case "GetStrategyOutputResponse":
            console.log(msg.Error);
            $("#LinesRow tr").remove();
            var GetStrategyScripts = msg;
            console.log(GetStrategyScripts);
            showOutputScript = GetStrategyScripts.Output;
            console.log(showOutputScript);
            if (showOutputScript.length !== 0) {
                for (var i = 0; i < showOutputScript.length; i++) {
                    LinesId[i] = showOutputScript[i].LineId;
                    EnabledLines[i] = showOutputScript[i].IsVisible;
                    ColorLines[i] = showOutputScript[i].ArgbColor;
                    LineName[i] = showOutputScript[i].LineName;
                    LineType[i] = showOutputScript[i].LineType;
                    LineSize[i] = showOutputScript[i].LineSize;
                    $("#LinesRow").append("<tr>" + "<td style='display: none;' id='linesId" + i + "'></td>" + "<td><input type='checkbox' id='enabled" + i + "'</td>" + "<td id='name" + i + "'></td>" + "<td>" + "<select name='LineType" + i + "'>" + "<option value='0'>Solid</option>" + "<option value='1'>Dash</option>" + "<option value='2'>Dot</option>" + "<option value='3'>Dash Dot</option>" + "<option value='4'>Dash Dot Dot</option>" + "</select>" + "</td>" + "<td>" + "<select name='LineSize" + i + "'>" + "<option value='1'>1</option>" + "<option value='2'>2</option>" + "<option value='3'>3</option>" + "<option value='4'>4</option>" + "<option value='5'>5</option>" + "<option value='6'>6</option>" + "<option value='7'>7</option>" + "<option value='8'>8</option>" + "<option value='9'>9</option>" + "<option value='10'>10</option>" + "</select>" + "</td>" + "<td>" + "<div class='input-group colorpicker-component floating-box' id='colorPicker" + i + "'>" + "<span class='input-group-addon cpborders'><i id='BuySellLineColor" + i + "'></i></span>" + "</div>" + "</td>" + "</tr>")
                }
            }
            for (var j = 0; j < LinesId.length; j++) {
                if (EnabledLines[j] === true) $("input#enabled" + j).attr("checked", true);
                else $("input#enabled" + j).attr("checked", false);
                $("td#linesId" + j).html(LinesId[j]);
                $("td#name" + j).html(LineName[j]);
                $("#colorPicker" + j).colorpicker({
                    color: "#" + fromArgb(ColorLines[j]),
                    format: 'rgba'
                });
                $("select[name='LineType" + j + "'] option[value='" + LineType[j] + "']").attr("selected", true);
                $("select[name='LineSize" + j + "'] option[value='" + LineSize[j] + "']").attr("selected", true)
            }
            break;
        case "ExportStrategyResponse":
            for (var key in msg) {
                if (key === "Strategies") bytes = msg[key]
            }
            console.log(bytes);
            var blob = new Blob([bytes]);
            saveAs(blob, "ExportStrategy.dat");
            break;
        case "GetLockedStrategyResponse":
            if (msg.Strategy !== null) {
                $("#passwordLockedStrategy").modal("hide");
                $("#createStrategy").modal("show");
                w2ui['strategyGrid'].set(msg.Strategy.ID, {
                    recid: msg.Strategy.ID,
                    enabled: msg.Strategy.IsEnabled,
                    name: msg.Strategy.Name,
                    indicator: msg.Strategy.PlotParameters.PlotIndicator,
                    signals: msg.Strategy.PlotParameters.PlotSignal,
                    fillbars: msg.Strategy.PlotParameters.FillBars
                });
                $("#strategyName").val(msg.Strategy.Name);
                $("#textBuyScript").val(msg.Strategy.BuyScript);
                $("#textSellScript").val(msg.Strategy.SellScript);
                $("#checkConditions").prop("checked", msg.Strategy.PlotParameters.CheckConditions);
                $("#fillBars").prop("checked", msg.Strategy.PlotParameters.FillBars);
                $("#PlotIndicatorID").prop("checked", msg.Strategy.PlotParameters.PlotIndicator);
                $("#PlotSignalID").prop("checked", msg.Strategy.PlotParameters.PlotSignal);
                $("#dropdownMenu1 span").replaceWith($(".buySellSignals .sellSignall .dropdown ul.dropdown-menu li[value='" + msg.Strategy.PlotParameters.PlotArrowTypeBuy + "']").html());
                $("#dropdownMenu2 span").replaceWith($(".buySellSignals .buySignall .dropdown ul.dropdown-menu li[value='" + msg.Strategy.PlotParameters.PlotArrowTypeSell + "']").html());
                $("select[name='SignalSizeBuy'] option:selected").text(msg.Strategy.PlotParameters.PlotSignalSizeBuy);
                $("select[name='SignalSizeSell'] option:selected").text(msg.Strategy.PlotParameters.PlotSignalSizeSell);
                console.log(fromArgb(PlotSignalColorBuy));
                $("#span2 span i").css("backgroundColor", "#" + fromArgb(msg.Strategy.PlotParameters.PlotSignalColorBuy));
                $("#span3 span i").css("backgroundColor", "#" + fromArgb(msg.Strategy.PlotParameters.PlotSignalColorSell));
                $("#span4 span i").css("backgroundColor", "#" + fromArgb(msg.Strategy.PlotParameters.BarBuyColor));
                $("#span5 span i").css("backgroundColor", "#" + fromArgb(msg.Strategy.PlotParameters.BarSellColor));
                if (msg.Strategy.Parameters.length !== 0) {
                    for (var i = 0; i < msg.Strategy.Parameters.length; i++) {
                        $("#LinesRow").append("<tr>" + "<td style='display:none;' id='linesId" + i + "'></td>" + "<td><input type='checkbox' id='enabled" + i + "'</td>" + "<td id='name" + i + "'></td>" + "<td>" + "<select name='LineType" + i + "'>" + "<option value='0'>Solid</option>" + "<option value='1'>Dash</option>" + "<option value='2'>Dot</option>" + "<option value='3'>Dash Dot</option>" + "<option value='4'>Dash Dot Dot</option>" + "</select>" + "</td>" + "<td>" + "<select name='LineSize" + i + "'>" + "<option value='1'>1</option>" + "<option value='2'>2</option>" + "<option value='3'>3</option>" + "<option value='4'>4</option>" + "<option value='5'>5</option>" + "<option value='6'>6</option>" + "<option value='7'>7</option>" + "<option value='8'>8</option>" + "<option value='9'>9</option>" + "<option value='10'>10</option>" + "</select>" + "</td>" + "<td>" + "<div class='input-group colorpicker-component floating-box' id='colorPicker" + i + "'>" + "<span class='input-group-addon cpborders'><i id='BuySellLineColor" + i + "'></i></span>" + "</div>" + "</td>" + "</tr>");
                        if (msg.Strategy.Parameters[i].IsVisible) $("input#enabled" + i).attr("checked", true);
                        else $("input#enabled" + i).attr("checked", false);
                        $("td#linesId" + i).html(msg.Strategy.Parameters[i].LineId);
                        $("td#name" + i).html(msg.Strategy.Parameters[i].LineName);
                        $("select[name='LineType" + i + "'] option[value='" + msg.Strategy.Parameters[i].LineType + "']").attr("selected", true);
                        $("select[name='LineSize" + i + "'] option[value='" + msg.Strategy.Parameters[i].LineSize + "']").attr("selected", true);
                        $("#colorPicker" + i + " span i").css("backgroundColor", "#" + fromArgb(msg.Strategy.Parameters[i].ArgbColor));
                        $("#colorPicker" + i).colorpicker({
                            color: fromArgb(msg.Strategy.Parameters[i].ArgbColor),
                            format: 'rgba'
                        })
                    }
                }
            } else {
                alert("Wrong Password!");
                $("#passwordLockedStrategy").modal("hide")
            }
            break;
        case "ApplyUStrategiesResponse":
        {
            EditedUserStrategyId = msg.Strategies;
            EditedUserName = msg.User;
            for (var i = 0; i < allUsersforNotifications.length; i++) {
                if (msg.User == allUsersforNotifications[i].UserName) {
                    allUsersforNotifications[i].UserStrategies = msg.Strategies;
                    console.log(allUsersforNotifications[i].UserStrategies);
                    break
                }
            }
            break
        }
        case "ApplyUScriptsResponse":
        {
            var ScriptsArray = [];
            for (var i = 0; i < allUsersforNotifications.length; i++) {
                if (msg.User == allUsersforNotifications[i].UserName) {
                    for (var j = 0; j < Scripts.length; j++) {
                        for (var x = 0; x < msg.Scripts.length; x++) {
                            if (Scripts[j].ID === msg.Scripts[x]) ScriptsArray[x] = Scripts[j]
                        }
                    }
                    allUsersforNotifications[i].UserScripts = ScriptsArray;
                    console.log(allUsersforNotifications[i].UserScripts);
                    break
                }
            }
            break
        }
        case "CreateScriptResponse":
        {
            var newScript = msg.Script;
            console.log(newScript);
            var ID = newScript.ID;
            var Name = newScript.Name;
            var IsFree = newScript.IsFree;
            Scripts = Scripts.concat(newScript);
            w2ui['ScriptGrid'].add({
                recid: ID,
                name: Name,
                isfree: IsFree
            });
            $("#scriptTable").append("<tr id='" + ID + "'><td style='display: none;'>" + ID + "</td><td class='tdScriptEnabled' id=" + ID + "><input type='checkbox' id='" + ID + "'></td><td class='tdScriptName'>" + Name + "</td></tr>");
            $("#grid_ScriptGrid_records input[type='checkbox']").attr("disabled", true);
            break
        }
        case "EditScriptResponse":
        {
            var editScript = msg.Script;
            console.log(editScript);
            if (newScript !== undefined) {
                Scripts = Scripts.concat(newScript)
            }
            for (var i = 0; i < Scripts.length; i++) {
                if (editScript.ID === Scripts[i].ID) Scripts.splice(i, 1, editScript)
            }
            w2ui['ScriptGrid'].set(editScript.ID, {
                recid: editScript.ID,
                name: editScript.Name,
                isfree: editScript.IsFree
            });
            $("#scriptTable tr[id='" + editScript.ID + "'] td.tdScriptName").html(editScript.Name);
            $("#grid_ScriptGrid_records input[type='checkbox']").attr("disabled", true);
            break
        }
        case "DeleteScriptResponse":
        {
            var deleteScriptId = msg.Script.ID;
            w2ui['ScriptGrid'].remove(deleteScriptId);
            console.log($("#scriptTable tr#" + deleteScriptId));
            $("#scriptTable tr#" + deleteScriptId).remove();
            for (var i = 0; i < Scripts.length; i++) {
                if (deleteScriptId === Scripts[i].ID) Scripts.splice(i, 1)
            }
            $("#grid_ScriptGrid_records input[type='checkbox']").attr("disabled", true);
            break
        }
        case "ValidScriptResponse":
        {
            if ((msg.Error === "" || msg.Error === undefined)) {
                if (flag2 === 0) {
                    $("#createScript").modal("hide");
                    createScript()
                }
                if (flag2 === 1) {
                    $("#editScriptModal").modal("hide")
                }
            } else {
                alert("Script generate an error!\n" + msg.Error)
            }
            break
        }
    }
}

function onError(evt) {
    console.log('ERROR: ' + evt.data)
}

function loginRequest() {
    var login = {
        Username: document.getElementById("loginUsername").value,
        Password: document.getElementById("loginPassword").value
    };
    websocket.send(JSON.stringify(login));
    console.log("SENT: " + " evt.data")
}

function getAllUsersRequest() {
    var getAllUsers = {
        Operation: "GetAllUsersRequest"
    };
    websocket.send(JSON.stringify(getAllUsers))
}

function getAllStrategiesRequest() {
    var getAllStrategies = {
        Operation: "GetAllStrategiesRequest"
    };
    websocket.send(JSON.stringify(getAllStrategies))
}

function getAllNotificationsRequest() {
    var getAllNotification = {
        Operation: "GetAllNotificationsRequest"
    };
    websocket.send(JSON.stringify(getAllNotification))
}

function getAllScriptRequest() {
    var getAllScripts = {
        Operation: "GetAllScriptsRequest"
    };
    websocket.send(JSON.stringify(getAllScripts))
}

function createUserRequest(Operation) {
    $('.modal-header h4').text('Create User');
    $('.modal-footer button:first-child').removeAttr('id');
    $('.modal-footer button:first-child').text('Create');
    var createUser = {
        User: {
            Username: document.getElementById("usrname").value,
            Email: document.getElementById("email").value,
            Password: document.getElementById("rpsw").value
        },
        Operation: Operation
    };
    websocket.send(JSON.stringify(createUser));
    console.log("SENT: " + " evt.data")
}

function removeUserRequest() {
    var id = $("tr.w2ui-selected").attr("recid");
    var record = w2ui['usersGrid'].get(id);
    var removeUser = {
        User: {
            Username: record.colUsername,
            Email: record.colEmail
        },
        Operation: "RemoveUserRequest"
    };
    websocket.send(JSON.stringify(removeUser))
}

function resetLockedStrategy() {
    lockedStrategyPassword = "";
    $("#unlock").attr("id", "lock");
    $("#lock").html("Lock");
    $("#lock").attr("onclick", "beforeLockStrategy()");
    var id = $('tr.w2ui-selected').attr('recid');
    console.log(id);
    w2ui['strategyGrid'].set(id, {
        enabled: true
    })
}

function GetLockedStrategyRequest() {
    if (validNewPassword === true && validRepeatNewPassword === true) {
        var counter = 0;
        $("#lock").attr("id", "unlock");
        $("#unlock").html("Unlock");
        $("#unlock").attr("data-target", "");
        $("#unlock").attr("onclick", "resetLockedStrategy()");
        lockedStrategyPassword = $("input#newPsw").val();
        $("#changeLockedStrategy").modal("hide");
        console.log(lockedStrategyPassword);
        var id = $('tr.w2ui-selected').attr('recid');
        w2ui['strategyGrid'].set(id, {
            enabled: false
        })
    }
    if ($("#newPsw").val() === "") $("#messageNewPassword").text("The field repeat password should not be empty");
    if ($("#repeatPsw").val() === "") $("#messageRepeatNewPassword").text("The field repeat password should not be empty")
}

function beforeEditUserStrategy() {
    console.log(EditedUserName);
    console.log(EditedUserStrategyId);
    if (newUserforNotifications !== undefined) allUsersforNotifications = allUsersforNotifications.concat(newUserforNotifications);
    $("#editUsrname").attr("disabled", true);
    var checks = $("#strategyTable input[type='checkbox']:checked");
    for (var i = 0; i < checks.length; i++) {
        checks[i].checked = false
    }
    var checks2 = $("#scriptTable input[type='checkbox']:checked");
    for (var i = 0; i < checks2.length; i++) {
        checks2[i].checked = false
    }
    console.log(allUsersforNotifications);
    forDatePicker();
    var id = $('tr.w2ui-selected').attr('recid');
    var record = w2ui['usersGrid'].get(id);
    if (id !== undefined) {
        for (var i = 0; i < allUsersforNotifications.length; i++) {
            if (allUsersforNotifications[i].UserName == record.colUsername) {
                userStrategies = allUsersforNotifications[i].UserStrategies;
                userScripts = allUsersforNotifications[i].UserScripts;
                break
            }
        }
        for (var key in userStrategies) {
            $("#editUserStrategy input[id='" + key + "']").prop("checked", true)
        }
        if (userScripts !== undefined) {
            console.log(userScripts);
            for (var i = 0; i < userScripts.length; i++) {
                console.log(userScripts[i].ID);
                var userScriptNotCheck = $("#editUserScript input[id='" + userScripts[i].ID + "']").not(":checked");
                for (var j = 0; j < userScriptNotCheck.length; j++) {
                    userScriptNotCheck[j].checked = true
                }
            }
        }
        if (userStrategies !== undefined) {
            for (var i = 0; i < userStrategies.length; i++) {
                var test = $("#editUserStrategy input[id='" + userStrategies[i].Strategy.ID + "']").not(":checked");
                for (var j = 0; j < test.length; j++) {
                    console.log(userStrategies[i].ExpirationDate);
                    $("#editUserScript input[id='data" + j + "']").val(userStrategies[i].ExpirationDate);
                    test[j].checked = true;
                    $("#editUserStrategy tr[id='" + userStrategies[i].Strategy.ID + "'] input[type='text']").val(ExpirationUserStrategies[j])
                }
            }
        }
        $("#userStrategy").attr('data-target', '#editUser');
        $('#editUsrname').val(record.colUsername);
        console.log(record.colUsername);
        $('.modal-header h4').text('Edit User');
        $('.modal-footer button:first-child').removeAttr('id');
        $('.modal-footer button:first-child').text('Edit')
    } else {
        alert('Please Select User');
        $("#userStrategy").attr('data-target', '')
    }
}

function beforeDeleteUser() {
    var id = $('tr.w2ui-selected').attr('recid');
    var record = w2ui['usersGrid'].get(id);
    if (id) {
        $("#deleteUser").attr('data-target', '#deleteUserInfo');
        $('.modal-header h4').text('Delete User');
        $('.modal-footer button:first-child').removeAttr('id');
        $('.modal-footer button:first-child').text('YES');
        document.getElementById('userName').innerHTML = record.colUsername
    } else {
        alert("Please Select User");
        $("#deleteUser").attr('data-target', '')
    }
}

function editUserStrategy() {
    var userStrategyId = {};
    var userStrategyDate = {};
    var checkStrategy = $("#editUserStrategy input[type='checkbox']");
    for (var i = 0; i < checkStrategy.length; i++) {
        if (checkStrategy[i].checked) {
            console.log($("#date" + i).val());
            userStrategyId[i] = checkStrategy[i].getAttribute("id");
            userStrategyDate[userStrategyId[i]] = $("#date" + i).val()
        }
    }
    var userScript = [];
    var checkScript = $("#editUserScript input[type='checkbox']");
    for (var j = 0; j < checkScript.length; j++) {
        if (checkScript[j].checked) {
            userScript[j] = checkScript[j].getAttribute("id")
        }
    }
    console.log(userScript);
    console.log(userStrategyDate);
    var editUserStrategy = {
        User: document.getElementById("editUsrname").value,
        Strategies: userStrategyDate,
        Operation: "ApplyUStrategiesRequest"
    };
    var editUserScript = {
        User: document.getElementById("editUsrname").value,
        Scripts: userScript,
        Operation: "ApplyUScriptsRequest"
    };
    console.log(editUserStrategy);
    websocket.send(JSON.stringify(editUserStrategy));
    websocket.send(JSON.stringify(editUserScript))
}

function resetCreateStrategyModal() {
    $("#LinesRow tr").remove();
    lockedStrategyPassword = "";
    buyArrowSignall = "0";
    sellArrowSignall = "0";
    $('#createStrategyForm').trigger('reset');
    $('#createStrategy .modal-header h4').text('Create Strategy');
    $("#createStrategy .modal-footer button:first-child").attr("id", "createEditStrategy");
    $("#createStrategy .modal-footer button:first-child").attr("onclick", "createStrategy('CreateStrategyRequest')");
    $('#createStrategy .modal-footer button:first-child').text('Create');
    $("button#unlock").html("Lock");
    $("button#unlock").attr("data-target", "#changeLockedStrategy");
    $("button#unlock").attr("onclick", "beforeLockStrategy()");
    $("button#unlock").attr("id", "lock");
    $("#checkConditions").attr("checked", false);
    $("#fillBars").attr("checked", false);
    $("#PlotIndicatorID").attr("checked", false);
    $("#PlotSignalID").attr("checked", false);
    $("#dropdownMenu1 span").replaceWith($(".buySellSignals .sellSignall .dropdown ul.dropdown-menu li[value='0']").html());
    $("#dropdownMenu2 span").replaceWith($(".buySellSignals .buySignall .dropdown ul.dropdown-menu li[value='0']").html());
    $("select[name='SignalSizeBuy'] option:selected").text("1");
    $("select[name='SignalSizeSell'] option:selected").text("1");
    $("#span2 span i").css("backgroundColor", "#12cf26");
    $("#span3 span i").css("backgroundColor", "#ff0000");
    $("#span4 span i").css("backgroundColor", "#12cf26");
    $("#span5 span i").css("backgroundColor", "#ff0000")
}

function createStrategy(Operation) {
    var rowsCount = $("#LinesRow tr").length;
    var Parameters = [];
    var StrategyName = document.getElementById("strategyName").value;
    var ScriptId = [];
    var ScriptEnabled = [];
    var ScriptIsBuy = [];
    var ScriptName = [];
    var ScriptColor = [];
    var ScriptLineType = [];
    var ScriptLineSize = [];
    var redScriptColorLines = [];
    var greenScriptColorLines = [];
    var blueScriptColorLines = [];
    var ScriptColorLinesHex = [];
    var ScriptColorLinesInteger = [];
    if (showOutputScript !== undefined) {
        for (var i = 0; i < showOutputScript.length; i++) {
            ScriptId[i] = showOutputScript[i].LineId;
            ScriptIsBuy[i] = showOutputScript[i].IsBuy;
            ScriptEnabled[i] = showOutputScript[i].IsVisible;
            ScriptName[i] = showOutputScript[i].LineName;
            ScriptColor[i] = showOutputScript[i].ArgbColor;
            ScriptLineType[i] = showOutputScript[i].LineType;
            ScriptLineSize[i] = showOutputScript[i].LineSize;
            console.log(ScriptColor[i])
        }
    } else {
        var rowsCount2 = $("#LinesRow tr");
        for (var x = 0; x < rowsCount2.length; x++) {
            ScriptId[x] = $("#LinesRow #linesId" + x).text()
        }
    }
    if (document.getElementById("strategyName").value !== "") {
        if (($("#textBuyScript").val() !== "") || ($("#textSellScript").val() !== "")) {
            if (flag !== 1) {
                for (var j = 0; j < ScriptId.length; j++) {
                    ScriptName[j] = $("#LinesRow #name" + j).text();
                    if ($("#LinesRow #enabled" + j).prop("checked")) ScriptEnabled[j] = true;
                    else ScriptEnabled[j] = false;
                    ScriptLineType[j] = $("#LinesRow select[name='LineType" + j + "'] option:selected").val();
                    ScriptLineSize[j] = $("#LinesRow select[name='LineSize" + j + "'] option:selected").val();
                    ScriptColor[j] = document.getElementById("BuySellLineColor" + j).style.backgroundColor.split(",");
                    redScriptColorLines[j] = parseInt(ScriptColor[j][0].replace(/\D+/g, ""));
                    greenScriptColorLines[j] = parseInt(ScriptColor[j][1].replace(/\D+/g, ""));
                    blueScriptColorLines[j] = parseInt(ScriptColor[j][2].replace(/\D+/g, ""));
                    ScriptColorLinesHex[j] = rgbToHex(redScriptColorLines[j], greenScriptColorLines[j], blueScriptColorLines[j]);
                    ScriptColorLinesInteger[j] = toArgb(ScriptColorLinesHex[j]);
                    Parameters[j] = {
                        ArgbColor: ScriptColorLinesInteger[j],
                        Index: 6,
                        IsBuy: ScriptIsBuy[j],
                        isVisible: ScriptEnabled[j],
                        LineId: ScriptId[j],
                        LineName: ScriptName[j],
                        LineSize: ScriptLineSize[j],
                        LineType: ScriptLineType[j]
                    }
                }
                console.log(Parameters);
                var rgbBarBuy = document.getElementById("buyBarColor").style.backgroundColor.split(",");
                var redBarBuy = parseInt(rgbBarBuy[0].replace(/\D+/g, ""));
                var greenBarBuy = parseInt(rgbBarBuy[1].replace(/\D+/g, ""));
                var blueBarBuy = parseInt(rgbBarBuy[2].replace(/\D+/g, ""));
                var rgbToHexBarBuy = rgbToHex(redBarBuy, greenBarBuy, blueBarBuy);
                var hexToIntBarBuy = toArgb(rgbToHexBarBuy.toString());
                var rgbBarSell = document.getElementById("sellBarColor").style.backgroundColor.split(",");
                var redBarSell = parseInt(rgbBarSell[0].replace(/\D+/g, ""));
                var greenBarSell = parseInt(rgbBarSell[1].replace(/\D+/g, ""));
                var blueBarSell = parseInt(rgbBarSell[2].replace(/\D+/g, ""));
                var rgbToHexBarSell = rgbToHex(redBarSell, greenBarSell, blueBarSell);
                var hexToIntBarSell = toArgb(rgbToHexBarSell.toString());
                var rgbSignalBuy = document.getElementById("signalColorBuy").style.backgroundColor.split(",");
                var redSignalBuy = parseInt(rgbSignalBuy[0].replace(/\D+/g, ""));
                var greenSignalBuy = parseInt(rgbSignalBuy[1].replace(/\D+/g, ""));
                var blueSignalBuy = parseInt(rgbSignalBuy[2].replace(/\D+/g, ""));
                var rgbToHexSignalBuy = rgbToHex(redSignalBuy, greenSignalBuy, blueSignalBuy);
                var hexToIntSignalBuy = toArgb(rgbToHexSignalBuy.toString());
                var rgbSignalSell = document.getElementById("signalColorSell").style.backgroundColor.split(",");
                var redSignalSell = parseInt(rgbSignalSell[0].replace(/\D+/g, ""));
                var greenSignalSell = parseInt(rgbSignalSell[1].replace(/\D+/g, ""));
                var blueSignalSell = parseInt(rgbSignalSell[2].replace(/\D+/g, ""));
                var rgbToHexSignalSell = rgbToHex(redSignalSell, greenSignalSell, blueSignalSell);
                var hexToIntSignalSell = toArgb(rgbToHexSignalSell.toString());
                PlotSignalSizeBuy = $("select[name='SignalSizeBuy'] option:selected").text();
                PlotSignalSizeSell = $("select[name='SignalSizeSell'] option:selected").text();
                var enabledStrategy;
                if (lockedStrategyPassword !== "") enabledStrategy = false;
                else enabledStrategy = true;
                if (Operation === "CreateStrategyRequest") ID = "";
                console.log(ID);
                var createStrategy = {
                    Strategy: {
                        ID: ID,
                        Name: document.getElementById("strategyName").value,
                        BuyScript: document.getElementById("textBuyScript").value,
                        SellScript: document.getElementById("textSellScript").value,
                        PlotParameters: {
                            PlotIndicator: $('#PlotIndicatorID').prop("checked"),
                            PlotSignal: $('#PlotSignalID').prop("checked"),
                            FillBars: $('#fillBars').prop("checked"),
                            CheckConditions: $('#checkConditions').prop("checked"),
                            BarBuyColor: hexToIntBarBuy,
                            BarSellColor: hexToIntBarSell,
                            PlotArrowTypeSell: sellArrowSignall,
                            PlotArrowTypeBuy: buyArrowSignall,
                            PlotSignalSizeSell: PlotSignalSizeSell,
                            PlotSignalColorSell: hexToIntSignalSell,
                            PlotSignalColorBuy: hexToIntSignalBuy,
                            PlotSignalSizeBuy: PlotSignalSizeBuy
                        },
                        Parameters: Parameters,
                        isEnabled: enabledStrategy,
                        Password: lockedStrategyPassword
                    },
                    Operation: Operation
                };
                console.log(createStrategy);
                websocket.send(JSON.stringify(createStrategy));
                $("#createStrategy .modal-footer button:first-child").attr("data-dismiss", "modal");
                console.log(flag)
            } else {
                alert("Please get script`s output first");
                $("#createStrategy .modal-footer button:first-child").attr("data-dismiss", "")
            }
        } else {
            alert("Script field cannot be empty");
            $("#createStrategy .modal-footer button:first-child").attr("data-dismiss", "")
        }
    } else {
        alert("Name field cannot be empty");
        $("#createStrategy .modal-footer button:first-child").attr("data-dismiss", "")
    }
}

function beforeDeleteStrategy() {
    var id = $('tr.w2ui-selected').attr('recid');
    var record = w2ui['strategyGrid'].get(id);
    if (id !== undefined) {
        $("#deleteStrategy").attr('data-target', '#deleteStrategyInfo');
        $("#deleteStrategyInfo .modal-footer button:first-child").attr("onclick", "deleteStrategy('DeleteStrategyRequest')");
        document.getElementById('deleteStrategyName').innerHTML = record.name;
        $('#deleteStrategyInfo .modal-header h4').text('Delete Strategy');
        $('#deleteStrategyInfo .modal-footer button:first-child').text('YES')
    } else {
        alert("Please Select Strategy");
        $("#deleteStrategy").attr('data-target', '')
    }
}

function beforeDeleteNotification() {
    var id = $("tr.w2ui-selected").attr("recid");
    var record = w2ui['notificationGrid'].get(id);
    if (id !== undefined) {
        $("#deleteNotification").attr('data-target', '#deleteNotificationInfo');
        document.getElementById('deleteNotificationName').innerHTML = record.name;
        $("#deleteNotificationInfo .modal-header h4").text("Delete Notification");
        $("#deleteNotificationInfo .modal-footer button:first-child").text("YES");
        $("#deleteNotificationInfo .modal-footer button:first-child").attr("onclick", "removeNotification('RemoveNotificationRequest','" + record.name + "','" + record.expiration + "')")
    } else {
        alert("Please Select Notification");
        $("#deleteNotification").attr('data-target', '')
    }
}

function beforeCreateUser() {
    $("#addUser .modal-header h4").text("Create User");
    $("#addUser .modal-footer button:first-child").attr("id", "createUser");
    $("#addUser .modal-footer button:first-child").text("Create");
    $("#addUser #usrname").val("");
    $("#addUser #email").val("");
    $("#addUser #psw").val("");
    $("#addUser #rpsw").val("")
}

function beforeLockStrategy() {
    var id = $("tr.w2ui-selected").attr("recid");
    $("#changeLockedStrategy .modal-header h4").text("Change Password");
    $("#changeLockedStrategy .modal-footer button:first-child").attr("onclick", "GetLockedStrategyRequest()");
    $("#changeLockedStrategy .modal-footer button:first-child").text("Apply");
    $("#lock").attr("data-target", "#changeLockedStrategy");
    $("#newPsw").val("");
    $("#repeatPsw").val("")
}

function deleteStrategy(Operation) {
    var id = $("tr.w2ui-selected").attr("recid");
    var record = w2ui['strategyGrid'].get(id);
    var deleteStrategy = {
        Strategy: {
            ID: record.recid,
            Name: record.name,
            PlotIndicator: record.indicator,
            PlotSignals: record.signals,
            PlotFillBars: record.fillbars
        },
        Operation: Operation
    };
    console.log(deleteStrategy);
    websocket.send(JSON.stringify(deleteStrategy))
}

function editStrategy() {
    flag = 0;
    lockedStrategyPassword = "";
    $("#LinesRow tr").remove();
    editCounter++;
    var id = $("tr.w2ui-selected").attr("recid");
    var record = w2ui['strategyGrid'].get(id);
    if (id) {
        console.log(record.enabled);
        var record = w2ui['strategyGrid'].get(id);
        $("#createStrategy .modal-header h4").text("Edit Strategy");
        $("#createStrategy .modal-footer button:first-child").attr("id", "createEditStrategy");
        $("#createStrategy .modal-footer button:first-child").attr("onclick", "createStrategy('EditStrategyRequest','" + id + "')");
        $("#createStrategy .modal-footer button:first-child").text("Edit");
        if (record.enabled !== false) {
            $("button#unlock").attr("id", "lock");
            $("button#lock").html("Lock");
            $("button#lock").attr("data-target", "#changeLockedStrategy");
            $("button#lock").attr("onclick", "beforeLockStrategy()");
            $("#myModal2").attr("data-target", "#createStrategy");
            console.log(newStrategiesArray);
            ArrayStrategies = Strategies;
            console.log(ArrayStrategies);
            for (var i = 0; i < ArrayStrategies.length; i++) {
                if (id === ArrayStrategies[i].ID) ObjectEditStrategy = ArrayStrategies[i]
            }
            console.log(ObjectEditStrategy);
            ID = ObjectEditStrategy.ID;
            console.log(ID);
            StrategyName = ObjectEditStrategy.Name;
            BuyScript = ObjectEditStrategy.BuyScript;
            SellScript = ObjectEditStrategy.SellScript;
            Parameters = ObjectEditStrategy.Parameters;
            if (Parameters.length !== 0) {
                for (var i = 0; i < Parameters.length; i++) {
                    LinesId[i] = Parameters[i].LineId;
                    EnabledLines[i] = Parameters[i].IsVisible;
                    LineName[i] = Parameters[i].LineName;
                    LineType[i] = Parameters[i].LineType;
                    LineSize[i] = Parameters[i].LineSize;
                    ColorLines[i] = Parameters[i].ArgbColor
                }
            }
            BarBuyColor = ObjectEditStrategy.PlotParameters.BarBuyColor;
            BarSellColor = ObjectEditStrategy.PlotParameters.BarSellColor;
            CheckConditions = ObjectEditStrategy.PlotParameters.CheckConditions;
            FillBarsEdit = ObjectEditStrategy.PlotParameters.FillBars;
            PlotIndicator = ObjectEditStrategy.PlotParameters.PlotIndicator;
            PlotSignal = ObjectEditStrategy.PlotParameters.PlotSignal;
            PlotArrowTypeBuy = ObjectEditStrategy.PlotParameters.PlotArrowTypeBuy;
            PlotArrowTypeSell = ObjectEditStrategy.PlotParameters.PlotArrowTypeSell;
            PlotSignalColorBuy = ObjectEditStrategy.PlotParameters.PlotSignalColorBuy;
            PlotSignalColorSell = ObjectEditStrategy.PlotParameters.PlotSignalColorSell;
            PlotSignalSizeBuy = ObjectEditStrategy.PlotParameters.PlotSignalSizeBuy;
            PlotSignalSizeSell = ObjectEditStrategy.PlotParameters.PlotSignalSizeSell;
            $("input#checkConditions").prop("checked", CheckConditions);
            $("input#fillBars").prop("checked", FillBarsEdit);
            $("input#PlotIndicatorID").prop("checked", PlotIndicator);
            $("input#PlotSignalID").prop("checked", PlotSignal);
            if (Parameters.length !== 0) {
                for (var i = 0; i < Parameters.length; i++) {
                    $("#LinesRow").append("<tr>" + "<td style='display:none;' id='linesId" + i + "'></td>" + "<td><input type='checkbox' id='enabled" + i + "'</td>" + "<td id='name" + i + "'></td>" + "<td>" + "<select name='LineType" + i + "'>" + "<option value='0'>Solid</option>" + "<option value='1'>Dash</option>" + "<option value='2'>Dot</option>" + "<option value='3'>Dash Dot</option>" + "<option value='4'>Dash Dot Dot</option>" + "</select>" + "</td>" + "<td>" + "<select name='LineSize" + i + "'>" + "<option value='1'>1</option>" + "<option value='2'>2</option>" + "<option value='3'>3</option>" + "<option value='4'>4</option>" + "<option value='5'>5</option>" + "<option value='6'>6</option>" + "<option value='7'>7</option>" + "<option value='8'>8</option>" + "<option value='9'>9</option>" + "<option value='10'>10</option>" + "</select>" + "</td>" + "<td>" + "<div class='input-group colorpicker-component floating-box' id='colorPicker" + i + "'>" + "<span class='input-group-addon cpborders'><i id='BuySellLineColor" + i + "'></i></span>" + "</div>" + "</td>" + "</tr>")
                }
                for (var i = 0; i < LinesId.length; i++) {
                    if (EnabledLines[i] === true) $("input#enabled" + i).attr("checked", true);
                    else $("input#enabled" + i).attr("checked", false);
                    $("td#linesId" + i).html(LinesId[i]);
                    $("td#name" + i).html(LineName[i]);
                    $("select[name='LineType" + i + "'] option[value='" + LineType[i] + "']").attr("selected", true);
                    $("select[name='LineSize" + i + "'] option[value='" + LineSize[i] + "']").attr("selected", true);
                    $("#colorPicker" + i + " span i").css("backgroundColor", "#" + fromArgb(ColorLines[i]))
                }
                for (var j = 0; j < LinesId.length; j++) {
                    $("#colorPicker" + j).colorpicker({
                        color: fromArgb(ColorLines[j]),
                        format: 'rgba'
                    })
                }
            }
            $("#strategyName").val(StrategyName);
            $("#textBuyScript").val(BuyScript);
            $("#textSellScript").val(SellScript);
            $("#dropdownMenu1 span").replaceWith($(".buySellSignals .sellSignall .dropdown ul.dropdown-menu li[value='" + PlotArrowTypeBuy + "']").html());
            $("#dropdownMenu2 span").replaceWith($(".buySellSignals .buySignall .dropdown ul.dropdown-menu li[value='" + PlotArrowTypeSell + "']").html());
            $("select[name='SignalSizeBuy'] option:selected").text(PlotSignalSizeBuy);
            $("select[name='SignalSizeSell'] option:selected").text(PlotSignalSizeSell);
            console.log(fromArgb(PlotSignalColorBuy));
            $("#span2 span i").css("backgroundColor", "#" + fromArgb(PlotSignalColorBuy));
            $("#span3 span i").css("backgroundColor", "#" + fromArgb(PlotSignalColorSell));
            $("#span4 span i").css("backgroundColor", "#" + fromArgb(BarBuyColor));
            $("#span5 span i").css("backgroundColor", "#" + fromArgb(BarSellColor))
        } else {
            $("button#lock").html("Unlock");
            $("button#lock").attr("data-target", "");
            $("button#lock").attr("onclick", "resetLockedStrategy()");
            $("button#lock").attr("id", "unlock");
            $("#passwordLockedStrategy").modal('show');
            $("#myModal2").attr("data-target", "");
            $("input#lockPsw").val("")
        }
    } else {
        alert("Please Select Strategy");
        $("#myModal2").attr("data-target", "")
    }
}

function exportStrategy() {
    var exportStrategy = {
        Strategy: {
            Operation: "ExportStrategyRequest"
        }
    };
    websocket.send(JSON.stringify(exportStrategy))
}

function importStrategy() {
    var importStrategy = {
        Strategy: {
            Operation: "ImportStrategyRequest"
        }
    };
    websocket.send(JSON.stringify(importStrategy));
    w2ui["strategyGrid"].clear()
}

function forDatePicker() {
    var date = new Date();
    var year = date.getFullYear();
    var month = date.getMonth() + 1;
    var day = date.getDate();
    var hours = date.getHours();
    var minutes = date.getMinutes();
    if (month < 10) month = "0" + month;
    if (day < 10) day = "0" + day;
    if (hours < 10) hours = "0" + hours;
    if (minutes < 10) minutes = "0" + minutes;
    var fullDate = year + "-" + month + "-" + day + " " + hours + ":" + minutes;
    $("#datepicker").val(fullDate);
    $(".datepicker2").val(fullDate);
    $("#datepicker").datetimepicker({
        format: 'yyyy-mm-dd hh:ii'
    });
    $(".datepicker2").datetimepicker({
        format: 'yyyy-mm-dd hh:ii'
    })
}

function configNotificationModal() {
    $("#createNotification .modal-header h4").text("Create Notification");
    $("#createNotification .modal-footer button:first-child").attr("id", "createNotification");
    $("#createNotification .modal-footer button:first-child").attr("onclick", "createNotification('CreateNotificationRequest')");
    $("#createNotification .modal-footer button:first-child").text("Create");
    forDatePicker();
    var count = 0;
    var count2 = 0;
    $("p.usersCheckbox input[type='checkbox']").attr("checked", false);
    $("p.usersCheckbox").on("mouseover", function() {
        $(this).css("cursor", "pointer")
    });
    $("p.usersCheckbox").on("click", function() {
        count2++;
        console.log(count2);
        var id = $(this).attr("id");
        if (count2 % 2 !== 0) $("p.usersCheckbox input[value='" + id + "']").attr("checked", true);
        else $("p.usersCheckbox input[value='" + id + "']").attr("checked", false)
    });
    $("#checkAll input[value='']").on("click", function() {
        count++;
        if (count % 2 !== 0) {
            for (var i = 0; i < $("p.usersCheckbox input[type='checkbox']").length; i++) {
                if ($("p.usersCheckbox input[type='checkbox']")[i].checked == false) $("p.usersCheckbox input[type='checkbox']")[i].checked = true;
                $("p.usersCheckbox input[type='checkbox']")[i].innerHTML
            }
        } else {
            for (var i = 0; i < $("p.usersCheckbox input[type='checkbox']").length; i++) {
                if ($("p.usersCheckbox input[type='checkbox']")[i].checked == true) $("p.usersCheckbox input[type='checkbox']")[i].checked = false
            }
        }
    })
}

function createNotification(Operation) {
    var Users = [];
    var usersCheck = $("#checkAll input[type='checkbox']:not(':first'):checked");
    var TextNotification = tinyMCE.activeEditor.getContent();
    for (var i = 0; i < usersCheck.length; i++) {
        if (usersCheck[i].checked) Users[i] = usersCheck[i].value
    }
    console.log(Users);
    if (document.getElementById("notificationName").value !== "") {
        if (Users.length !== 0) {
            forDatePicker();
            var createNotification = {
                Notification: {
                    ID: "",
                    Name: document.getElementById("notificationName").value,
                    Text: TextNotification,
                    ExpirationDate: $("input#datepicker").val(),
                },
                Users: Users,
                Operation: Operation
            };
            websocket.send(JSON.stringify(createNotification));
            $("#createNotification .modal-footer button:first-child").attr("data-dismiss", "modal")
        } else {
            $("#createNotification .modal-footer button:first-child").attr("data-dismiss", "");
            alert("You should select users to send Notification")
        }
    } else {
        alert("Notification name cannot be empty");
        $("#createNotification .modal-footer button:first-child").attr("data-dismiss", "")
    }
}

function removeNotification(Operation, Name, Expiration) {
    var removeNotification = {
        Notification: {
            ID: "",
            Name: Name,
            Text: "",
            ExpirationDate: Expiration
        },
        Operation: Operation
    };
    websocket.send(JSON.stringify(removeNotification))
}

function showScript() {
    var buyScript = document.getElementById("textBuyScript").value;
    var sellScript = document.getElementById("textSellScript").value;
    if (buyScript !== "" || sellScript !== "") {
        var showScripts = {
            StrategyName: document.getElementById("strategyName").value,
            buyScript: buyScript,
            sellScript: sellScript,
            Operation: "GetStrategyOutputRequest"
        };
        console.log(showScripts);
        websocket.send(JSON.stringify(showScripts));
        flag = 0
    } else alert("Script field cannot be empty")
}

function saveStrategyLinesOrder() {
    var StrategyId = {};
    var Dictionary = [];
    var Records = w2ui['strategyGrid'].records;
    for (var i = 0; i < Records.length; i++) {
        for (var key in Records[i]) {
            if (key === "recid") {
                Dictionary[i] = i;
                StrategyId[i] = Records[i][key]
            }
        }
    }
    var StrategiesOrder = {
        Dictionary: Dictionary,
        StrategiesOrder: StrategyId,
        Operation: "ChangeStrategiesOrderRequest"
    };
    websocket.send(JSON.stringify(StrategiesOrder))
}

function beforeCreateScript() {
    $('.modal-header h4').text('Create StockScanner Script');
    $('.modal-footer button:first-child').text('Apply');
    $("#createScript input#ScriptName").val("");
    $("#createScript input[type='checkbox']").prop("checked", false);
    $("#createScript textarea").val("")
}

function createScript() {
    var IsFree = $("#createScript input[type='checkbox']").prop("checked");
    var Name = $("#createScript input#ScriptName").val();
    var Script = $("#createScript textarea").val();
    var Script2 = {
        Script: {
            ID: "",
            IsFree: IsFree,
            Name: Name,
            Script: Script
        },
        Operation: "CreateScriptRequest"
    };
    websocket.send(JSON.stringify(Script2))
}

function beforeEditScript() {
    var id = $("tr.w2ui-selected").attr("recid");
    var editObjectScript;
    if (id !== undefined) {
        $("button#editScript").attr("data-target", "#editScriptModal");
        $('.modal-header h4').text('Edit StockScanner Script');
        $('.modal-footer button:first-child').text('Apply');
        for (var i = 0; i < Scripts.length; i++) {
            if (id === Scripts[i].ID) editObjectScript = Scripts[i]
        }
        $("#EditedScriptName").val(editObjectScript.Name);
        $("#editScriptModal textarea").val(editObjectScript.Script);
        $("#editScriptModal input[type='checkbox'").prop("checked", editObjectScript.IsFree)
    } else {
        alert("Please Select Script");
        $("button#editScript").attr("data-target", "")
    }
}

function editScript() {
    var id = $("tr.w2ui-selected").attr("recid");
    var IsFree = $("#editScriptModal input[type='checkbox']").prop("checked");
    var Name = $("#editScriptModal input#EditedScriptName").val();
    var Script = $("#editScriptModal textarea").val();
    var Script = {
        Script: {
            ID: id,
            IsFree: IsFree,
            Name: Name,
            Script: Script
        },
        Operation: "EditScriptRequest"
    };
    websocket.send(JSON.stringify(Script))
}

function beforeDeleteScript() {
    var id = $("tr.w2ui-selected").attr("recid");
    if (id !== undefined) {
        $("button#deleteScript").attr("data-target", "#deleteScriptInfo");
        $('.modal-header h4').text('Delete Script');
        $('.modal-footer button:first-child').text('Yes');
        var record = w2ui['ScriptGrid'].get(id);
        $("#deleteScriptName").html(record.name)
    } else {
        alert("Please Select Script");
        $("button#deleteScript").attr("data-target", "")
    }
}

function deleteScript() {
    var id = $("tr.w2ui-selected").attr("recid");
    if (id) {
        var delScript = {
            Script: {
                ID: id
            },
            Operation: "DeleteScriptRequest"
        };
        websocket.send(JSON.stringify(delScript));
        $("#deleteScriptInfo .modal-footer button:first-child").attr("data-dismiss", "modal")
    }
}