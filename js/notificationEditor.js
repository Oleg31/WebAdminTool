/**
 * Created by Oleg on 22.02.2017.
 */

//For text editor (Notification)
tinymce.init({
    selector: 'textarea.editor',
    height: 190,
    menubar: false,
    plugins: [
        'advlist autolink lists link image charmap print preview anchor',
        'searchreplace visualblocks code fullscreen',
        'insertdatetime media table contextmenu paste code',
        'textcolor'
    ],
    //toolbar: 'styleselect | bold italic | alignleft aligncenter alignright alignjustify | outdent indent | link',
    toolbar: 'fontselect | fontsizeselect | bold,italic,underline,sub,sup,hr,| forecolor backcolor | link | alignleft aligncenter alignright alignjustify | bullist outdent indent',
    content_css: '//www.tinymce.com/css/codepen.min.css'
});